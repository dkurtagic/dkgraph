//
//  AppDelegate.h
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

