//
//  DKGraph.h
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DKGraphNode.h"
#import "DKGraphEdge.h"

@interface DKGraph : NSObject
{
    NSMutableArray *_nodes;
    NSMutableArray *_edges;
    NSString *_graphID;
}

@property (nonatomic, strong) NSMutableArray *nodes;
@property (nonatomic, strong) NSMutableArray *edges;
@property (nonatomic, strong) NSString *graphID;
@property (nonatomic, assign, getter = isDirected) BOOL directed;

- (void)printNodesDeg:(DKGraph*)graph;
- (void)breadthFirstSearch:(DKGraph*)graph;
@end
