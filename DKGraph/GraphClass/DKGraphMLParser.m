//
//  DKGraphMLParser.m
//  DKGraph
//
//  Created by Dino Kurtagić on 11/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import "DKGraphMLParser.h"
#import "NSDictionary+Extras.h"
@implementation DKGraphMLParser
{
    NSMutableString *_mElementValue;
    DKGraphNode *node;
    DKGraphEdge *edge;
}

- (DKGraph*)parseGraphFromData:(NSData *)data
{
    NSXMLParser *parser = nil;
    
    if (data != nil)
        parser = [[NSXMLParser alloc] initWithData:data];
    else
        return nil;
    
    _items = [NSMutableArray new];
    
    [parser setDelegate:self];
    
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    
    [parser parse];
    
    
    for(DKGraphEdge *edgeItem in graph.edges)
    {
        for(int i = 0; i<graph.nodes.count; i++)
        {
            if([edgeItem.sourceNode isEqualToString:((DKGraphNode*)[graph.nodes objectAtIndex:i]).nodeID])
            {
                [((DKGraphNode *)[graph.nodes objectAtIndex:i]).edgeConnections addObject:edgeItem];
            }
        }
//        for(DKGraphNode *nodeItem in graph.nodes)
//        {
//            if([edgeItem.sourceNode isEqualToString:nodeItem.nodeID])
//            {
//                [node.edgeConnections addObject:edgeItem];
//            }
//        }
    }
    
    return graph;
}

#pragma mark - NSXMLParserDelegate


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (string != nil)
    {
        [_mElementValue appendString:string];
    }
    else
    {
        _error = [NSError errorWithDomain:@"Parsing error! Appending nil value." code:-1 userInfo:nil];
        NSLog(@"Parsing error! Appending nil value.");
        [parser abortParsing];
    }
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    _error = parseError;
    NSLog(@"%@", [NSString stringWithFormat:@"Parsing error code %li, %@, at line: %li, column: %li", (long)[parseError code], [[parser parserError] localizedDescription], (long)[parser lineNumber], (long)[parser columnNumber]]);
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    _mElementValue = [[NSMutableString alloc] init];
    _elementValue = nil;
    _elementName = [NSString stringWithString:elementName];
    _attributesDict = attributeDict;
    
    [self didStartElement];
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    _elementName = [NSString stringWithString:elementName];
    _elementValue = [[NSString stringWithString:_mElementValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [self didEndElement];
}


- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
{
    _mElementValue = [[NSMutableString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
}


#pragma mark - Methods to override


- (void)didStartElement
{
    ifElement(@"graph")
    {
        graph = [DKGraph new];
        if([((NSString*)[_attributesDict parseField:@"edgedefault" withClass:[NSString class]]) isEqualToString:@"directed"])
            graph.directed = 1;
        else
            graph.directed = 0;
        
        graph.graphID = [_attributesDict parseField:@"id" withClass:[NSString class]];
    }
    elifElement(@"node")
    {
        node = [DKGraphNode new];
        node.nodeID = [_attributesDict parseField:@"id" withClass:[NSString class]];
    }
    elifElement(@"edge")
    {
        edge = [DKGraphEdge new];
        edge.edgeID = [_attributesDict parseField:@"id" withClass:[NSString class]];
        edge.sourceNode = [_attributesDict parseField:@"source" withClass:[NSString class]];
        edge.targetNode = [_attributesDict parseField:@"target" withClass:[NSString class]];
    }
}


- (void)didEndElement
{
    ifElement(@"edge")
    {
        [graph.edges addObject:edge];
        edge = nil;
    }
    ifElement(@"node")
    {
        [graph.nodes addObject:node];
        node = nil;
    }
    elifElement(@"data")
    {
        if(edge)
        {
            NSLog(@"%f", [_elementValue doubleValue]);
            edge.weight = [_elementValue doubleValue];
        }
    }
}


#pragma mark - itemsArray getter

- (NSArray *)itemsArray
{
    return [NSArray arrayWithArray:_items];
}

#pragma mark - graphItem geter
- (DKGraph*)graphItem
{
    return graph;
}
@end
