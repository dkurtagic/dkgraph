//
//  HomeViewController.m
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import "HomeViewController.h"
#import "DKGraphMLParser.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    DKGraphMLParser *parser = [DKGraphMLParser new];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Graph" ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
    DKGraph *graph = [parser parseGraphFromData:xmlData];
    NSLog(@"%@", graph);
    [graph printNodesDeg:graph];
    [graph breadthFirstSearch:graph];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
