//
//  Constants.h
//  Televend
//
//  Created by Dino Kurtagić on 12/10/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//
#import "UIAlertView+Blocks.h"
#import "DLanguage.h"

//===== DataSource

#define kRequestErrorRequestIsNull 10000
#define kRequestErrorResponseNot200 10001
#define kParseError 10002
#define kTimeoutInterval 60.0

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)