//  NSDictionary+Extras.h
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Samurai Digital. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSDictionary (Extras)
-(id)parseField:(NSString *)fieldKey withClass:(Class)fieldClass;
@end
