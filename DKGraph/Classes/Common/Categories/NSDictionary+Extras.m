//  NSDictionary+Extras.m
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Samurai Digital. All rights reserved.



#import "NSDictionary+Extras.h"



@implementation NSDictionary (Extras)

-(id)parseField:(NSString *)fieldKey withClass:(Class)fieldClass
{
    id field = [self objectForKey:fieldKey];
    return [field isKindOfClass:fieldClass] ? field : nil;
}

@end
